CREATE TABLE IF NOT EXISTS Property(
  Property_ID INTEGER PRIMARY KEY AUTOINCREMENT,
  Name VARCHAR(20) UNIQUE,
  Is_Active BOOLEAN,
  Description VARCHAR(100),
  Electric_Price INT(5),
  Water_Price INT(5)
);

CREATE TABLE IF NOT EXISTS Rooms(
  Room_Number VARCHAR(5) PRIMARY KEY ,
  Is_Active BOOLEAN,
  Owner VARCHAR(20),
  FK_Property_ID INT(2),
  Price INT(7),
  Electric_Meter INT(6),
  Water_Meter INT(6),
  FOREIGN KEY(FK_Property_ID) REFERENCES Property(Property_ID) ON DELETE CASCADE);

CREATE TABLE IF NOT EXISTS Invoices(Invoice_ID INTEGER PRIMARY KEY AUTOINCREMENT,
  FK_Room_Number VARCHAR(5),
  Type VARCHAR(5),
  Begin_Date DATE,
  End_Date DATE,
  Begin_Electric_Meter INT(6),
  End_Electric_Meter INT(6),
  Begin_Water_Meter INT(6),
  End_Water_Meter INT(6),
  Cash_Due REAL(7,2),
  Cash_Paid REAL(7,2),
  Is_Paid BOOLEAN, FOREIGN KEY(FK_Room_Number) REFERENCES Rooms(Room_Number) ON DELETE CASCADE);
--======================================================================
INSERT INTO Property (
  Name,
  Is_Active,
  Description,
  Electric_Price,
  Water_Price
) VALUES (
  'Near RP',
  true,
  '4PM collection',
  1000,
  1800
);

--========================================================================
INSERT INTO Rooms (
  Room_Number,
  Is_Active,
  Owner,
  FK_Property_ID,
  Date_In,
  Price,
  Electric_Meter,
  Water_Meter
) VALUES (
  'A1', true, 'Bob', 0, '2018-12-30', 50, 10, 20
);

INSERT INTO Rooms (
  Room_Number,
  Is_Active,
  Owner,
  FK_Property_ID,
  Date_In,
  Price,
  Electric_Meter,
  Water_Meter
) VALUES (
  'A2', true, 'Yarn', 0, '2018-10-30', 70, 30, 10
);

UPDATE Rooms SET Electric_Meter = 0 WHERE Room_ID = 2;

--========================================================================
INSERT INTO Invoices (
  Invoice_ID,
  FK_Room_Number,
  Type,
  Begin_Date,
  End_Date,
  Begin_Electric_Meter,
  End_Electric_Meter,
  Begin_Water_Meter,
  End_Water_Meter,
  Cash_Due
) VALUES (
  4,
  'A2',
  'NORM',
  '2018-2-1',
  '2018-3-1',
  20, 30,
  20, 25,
  '50$10000R'
);

UPDATE Invoices
  SET Cash_Paid = 25000, Is_Paid = TRUE
  WHERE Invoice_ID = 2;




Input: Current Meters
SELECT Invoices.Invoice_ID, Invoices.Type, Invoices.End_Date, Invoices.End_Electric_Meter, Invoices.End_Water_Meter, Rooms.Is_Active, Rooms.FK_Property_ID, Rooms.Electric_Meter, Rooms.Water_Meter, Property.Property_ID, Property.Electric_Price, Property.Water_Price FROM Invoices INNER JOIN Rooms ON Rooms.Room_Number = Invoices.FK_Room_Number INNER JOIN Property ON Rooms.FK_Property_ID = Property.Property_ID WHERE FK_Room_Number = ? ORDER BY Invoice_ID DESC LIMIT 1;

select Invoices.*, Rooms.* from Invoices INNER JOIN Rooms ON Rooms.Room_Number = Invoices.FK_Room_Number where FK_Room_Number = 'A1' ORDER BY Invoice_ID DESC LIMIT 1;

SELECT Is_Active from Rooms WHERE Room_Number = "A1";
SELECT MAX(Invoice_ID) AS Invoice_ID, TYPE  FROM Invoices WHERE FK_Room_Number = "A1";
UPDATE Rooms SET Is_Active = true WHERE Room_Number = 'A1';


-- GET THE LATEST MANY FROM DISTINCT ONE
SELECT Rooms.*, Invoices.* FROM Rooms INNER JOIN Invoices ON Invoices.Invoice_ID = (SELECT Invoice_ID FROM Invoices WHERE Invoices.FK_Room_Number = Rooms.Room_Number ORDER BY Invoice_ID DESC LIMIT 1);

NewUser: If(!Is_Active && (Last_Invoice_Type == "END" || NULL))
          Update Room Meters, Is_Active = true;
          Make Invoice  with TYPE "BOOK", Default 50$ and 30000R
Normal: if(Is_Active && Last_Invoice_Type == 'BOOK')
          Make Invoice with TYPE "FIRST", Currentmeter - meter set in room
        if(Is_Active && Last_Invoice_Type != "BOOK")
          Make Invoice with TYPE='NORM', Current Meter-Last_Month_Invoice
UserLeave(Date, Current_Meter){
  Set Room Is_Active to FALSE
  Make Invocie with TYPE="LEAVE", CurrentMeter-MeterLastInvoice
  Calculate give back ammount= 50/30*Day Elapsed since last invoice + Usage * Price Rates
  Give back = 50$30000R - Above result
}
