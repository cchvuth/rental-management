import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { NativeStorage } from '@ionic-native/native-storage';
import { SQLite} from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

import { InputPage } from '../pages/input/input';
import { CollectPage } from '../pages/collect/collect';
import { PropertyPage } from '../pages/property/property';
import { SettingsPage } from '../pages/settings/settings';
import { PropertyShowPage } from '../pages/property-show/property-show';
import { PropertyFormPage } from '../pages/property-form/property-form';

import { RoomFormPage } from '../pages/room-form/room-form';
import { RoomShowPage } from '../pages/room-show/room-show';
import { RoomEditPage } from '../pages/room-edit/room-edit';

import { InvoiceFormPage } from '../pages/invoice-form/invoice-form';
import { InvoiceOverridePage } from '../pages/invoice-override/invoice-override';

import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    InputPage,
    CollectPage,
    PropertyPage,
    SettingsPage,
    PropertyFormPage,
    PropertyShowPage,
    RoomFormPage,
    RoomShowPage,
    RoomEditPage,
    InvoiceFormPage,
    InvoiceOverridePage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InputPage,
    CollectPage,
    PropertyPage,
    SettingsPage,
    PropertyFormPage,
    PropertyShowPage,
    RoomFormPage,
    RoomShowPage,
    RoomEditPage,
    InvoiceFormPage,
    InvoiceOverridePage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    NativeStorage,
    Toast,
    SQLitePorter,
    File,
    SocialSharing,
    FileChooser,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
