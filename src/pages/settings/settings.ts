// Property Setting
import { Component} from '@angular/core';
import { NavController, NavParams, ViewController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  settings = {
    Property_ID: null,
    Name: null,
    Electric_Price: null,
    Water_Price: null
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    public viewCtrl: ViewController
  ) {
    this.get_current_settings(this.navParams.data.Property_ID);
  }

  get_current_settings(p_id){
    // connect to db
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('SELECT Property_ID, Name, Electric_Price, Water_Price FROM Property WHERE Property_ID = ?', [p_id])
        .then(res => {
          this.settings.Name = res.rows.item(0).Name;
          this.settings.Electric_Price = res.rows.item(0).Electric_Price;
          this.settings.Water_Price = res.rows.item(0).Water_Price;
          this.settings.Property_ID = p_id;
        })
        .catch(e => console.log(e));

      })
      .catch(e => console.log(e));
  }

  save_settings(Property_ID){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('UPDATE Property SET Name = ?, Electric_Price = ?, Water_Price = ? WHERE Property_ID = ?', [
          this.settings.Name,
          this.settings.Electric_Price,
          this.settings.Water_Price,
          this.settings.Property_ID
        ]).then(res => {
          this.toast.show('Settings Saved', '5000', 'center').subscribe();
          this.viewCtrl.dismiss();
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
        });

      }).catch(e => {
      this.toast.show(e, '5000', 'center').subscribe();
    });
  }

}
