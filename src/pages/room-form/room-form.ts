// Adding Room
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the RoomFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-room-form',
  templateUrl: 'room-form.html',
})
export class RoomFormPage {

    Rooms: any = {
      Room_Number : '',
      Is_Active : false,
      FK_Property_ID : '',
      Price: null,
      Electric_Meter: 0,
      Water_Meter: 0
    };
    Prefix: string = null;
    Is_Multiple: boolean = false;
    Start: number = 1;
    End: number = 1;
    constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private sqlite: SQLite,
      private nativeStorage: NativeStorage,
      private toast: Toast,
    ) {
    }

    add_rooms(){
      this.nativeStorage.getItem('Selected_Property')
      .then(
        data => {
          this.Rooms.FK_Property_ID = data.Property_ID;
          this.sqlite.create({
            name: 'data.db',
            location: 'default'
          })
            .then((db: SQLiteObject) => {
              if(!this.Is_Multiple){
                db.executeSql(
                  'INSERT INTO Rooms(Room_Number, Is_Active, FK_Property_ID, Price, Electric_Meter, Water_Meter) VALUES(?,?,?,?,?,?)',
                  [
                    this.Rooms.Room_Number,
                    this.Rooms.Is_Active,
                    this.Rooms.FK_Property_ID,
                    this.Rooms.Price,
                    this.Rooms.Electric_Meter,
                    this.Rooms.Water_Meter
                  ])
                .then(res => {
                    this.toast.show('Room Added', '5000', 'center').subscribe();
                    this.navCtrl.parent.select(1);
                  })
                  .catch(e => {
                    this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
                  });
              } else {
                for(var i = this.Start; i<=this.End; i++){
                  db.executeSql(
                    'INSERT INTO Rooms(Room_Number, Is_Active, FK_Property_ID, Price, Electric_Meter, Water_Meter) VALUES(?,?,?,?,?,?)',
                    [
                      this.Prefix + i,
                      this.Rooms.Is_Active,
                      this.Rooms.FK_Property_ID,
                      this.Rooms.Price,
                      this.Rooms.Electric_Meter,
                      this.Rooms.Water_Meter
                    ])
                  .then(res => {
                      this.toast.show('Room Added', '5000', 'center').subscribe();
                    })
                    .catch(e => {
                      this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
                    });
                }
                return this.navCtrl.parent.select(1);
              }



            })
            .catch(e => console.log(JSON.stringify(e)));
        },
        error => console.error(error)
      );

    }

  }
