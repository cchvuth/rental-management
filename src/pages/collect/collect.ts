// Collection Tab
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { NativeStorage } from '@ionic-native/native-storage';
import { Toast } from '@ionic-native/toast';
import { InvoiceOverridePage } from '../invoice-override/invoice-override';

@Component({
  selector: 'page-collect',
  templateUrl: 'collect.html'
})
export class CollectPage {

  Data:any = [];
  Property_ID: number;
  Property_Name: string;
  Exchange_Rate: number;
  // Sum at header
  SumRent:number = 0;
  SumUtil:number = 0;
  Cash_Got:number = 0;
  // For Date Num to 3 Char
  Month = ['NA', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  Payment_Remained: number = 0;
  constructor(
    public navCtrl: NavController,
    private sqlite: SQLite,
    private nativeStorage: NativeStorage,
    private alertCtrl: AlertController,
    private toast: Toast,
    public loadingCtrl: LoadingController
  ) {

  }

  ionViewWillEnter() {
    this.get_data(null);
  }

  detail_view(invoice_index){
    // swap value per press
    this.Data[invoice_index].detail_view ?
    this.Data[invoice_index].detail_view = false
    :
    this.Data[invoice_index].detail_view = true;
  }

  invoice_override(Invoice_ID){
    this.navCtrl.push(InvoiceOverridePage, {
      Invoice_ID:Invoice_ID
    })
  }

  toast_remain() {
    this.toast.show('Payment remained: '+ this.Payment_Remained, '5000', 'center').subscribe();
  }

  get_data(refresher){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    // Get Exchange Rate
    this.nativeStorage.getItem('Exchange_Rate')
    .then(
      data => {
        this.Exchange_Rate = Number(data.USD_KHR)/1000; // 1K style
      },
      error => console.error(error)
    );
    // Get Property_ID selected from Property Tab
    this.nativeStorage.getItem('Selected_Property')
    .then(
      data => {
        this.Property_ID = data.Property_ID;
        this.Property_Name = data.Name;

        // get data from db
        this.sqlite.create({
          name: 'data.db',
          location: 'default'
        })
        .then((db: SQLiteObject) => {

          db.executeSql('SELECT Rooms.Room_ID, Rooms.Room_Number, Rooms.Price, Invoices.Invoice_ID, strftime(\'%d/%m/%Y\', Invoices.End_Date) AS End_Date, Invoices.Type, Invoices.Cash_Due, Invoices.Cash_Paid, Invoices.Is_Paid FROM Rooms INNER JOIN Invoices ON Invoices.Invoice_ID = (SELECT Invoice_ID FROM Invoices WHERE Invoices.FK_Room_ID = Rooms.Room_ID ORDER BY Invoice_ID DESC LIMIT 1) WHERE Rooms.FK_Property_ID = ?;', [this.Property_ID])
          .then(res => {
            // clear Data
            this.Data = [];
            // reset Header Sums
            this.SumUtil = 0;
            this.SumRent = 0;
            this.Cash_Got = 0;
            this.Payment_Remained = res.rows.length;
            // Push every record into Data array
            for(var i=0; i<res.rows.length; i++) {
              this.Data.push(res.rows.item(i));
              this.Data[i].detail_view = false; // for hiding and showing paid and remain
              this.Data[i].Price = Number(this.Data[i].Price);
              this.Data[i].Cash_Due = Number(Number(this.Data[i].Cash_Due).toFixed(2));
              this.Data[i].Cash_Paid = Number(Number(this.Data[i].Cash_Paid).toFixed(2)); // set precision
              // Convert USD Due to Riel with precision .00
              this.Data[i].Cash_Due_Riel =
              (this.Data[i].Cash_Due * this.Exchange_Rate).toFixed(1) + 'K';
              // less means user did not stay for a full month, give back booking cash

              // cash Got calculation in Riel
              this.Data[i].Cash_Got_Riel = (this.Data[i].Cash_Paid * this.Exchange_Rate).toFixed(1) + 'K';

              // cash remain
              this.Data[i].Cash_Remain_Riel =
              ((this.Data[i].Cash_Due - this.Data[i].Cash_Paid) * this.Exchange_Rate).toFixed(1);

              if(this.Data[i].Cash_Due > this.Data[i].Price){
                // Room_Price|Utility style
                this.Data[i].Mix_Due =
                this.Data[i].Price + '|' +
                ((this.Data[i].Cash_Due - this.Data[i].Price) * this.Exchange_Rate).toFixed(1) + 'K';

                this.Data[i].Mix_Got =
                (this.Data[i].Cash_Paid <= this.Data[i].Price)?
                this.Data[i].Cash_Paid + '|0K'
                :
                this.Data[i].Price + '|' + ((this.Data[i].Cash_Paid - this.Data[i].Price) * this.Exchange_Rate).toFixed(1) + 'K';

                this.Data[i].Mix_Remain =
                (this.Data[i].Cash_Paid <= this.Data[i].Price)?
                (this.Data[i].Price - this.Data[i].Cash_Paid) + '|' + ((this.Data[i].Cash_Due - this.Data[i].Price) * this.Exchange_Rate).toFixed(1) + 'K'
                :
                '0|' + ((this.Data[i].Cash_Due - this.Data[i].Cash_Paid) * this.Exchange_Rate).toFixed(1) + 'K';

              }

              // Calculate Sum of All Invoice Rents And Utility
              this.SumRent +=this.Data[i].Price;
              if(this.Data[i].Cash_Due-this.Data[i].Price>=0){
                this.SumUtil += (this.Data[i].Cash_Due-this.Data[i].Price);
              }
              if(this.Data[i].Type != 'LEAVE' && this.Data[i].Is_Paid == 'true') {
                this.Payment_Remained--;
                this.Cash_Got += this.Data[i].Cash_Paid;
              }
              // Get the First 3 Letter of the Month With Year
              if(this.Data[i].End_Date){
                this.Data[i].End_Date = this.Data[i].End_Date.substr(0,2) + ', ' + this.Month[Number(this.Data[i].End_Date.substr(3,2))] + ' ' + this.Data[i].End_Date.substr(6,4);
              }

            }
            this.SumUtil *= this.Exchange_Rate;
            this.SumUtil = Number(this.SumUtil.toFixed(1));
            loading.dismiss();
            if(refresher) refresher.complete();
          })
          .catch(e => console.log(JSON.stringify(e)));

        })
        .catch(e => console.log(JSON.stringify(e)));

      },
      error => console.error(error)
    ).catch(e => this.toast.show('Please select a property to begin with', '5000', 'center').subscribe())
  }

  add_payment(Is_In_Full, Partial_Recieved, Invoice_ID, Cash_Due){
    // For Full Payment, No need to get input
    if(Is_In_Full){
      let alert = this.alertCtrl.create({
        message: 'Full Payment',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Confirm!',
            handler: data => {
              this.sqlite.create({
                name: 'data.db',
                location: 'default'
              })
              .then((db: SQLiteObject) => {

                db.executeSql('UPDATE Invoices SET Cash_Paid = ?, Is_Paid = ? WHERE Invoice_ID = ?', [
                  Partial_Recieved,
                  true,
                  Invoice_ID
                ]).then(res => {
                  this.toast.show('Payment Sucessful!', '5000', 'center').subscribe();
                  this.get_data(null);
                  this.navCtrl.parent.select(2);
                })
                .catch(e => {
                  this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
                });


              }).catch(e => {
                this.toast.show(e, '5000', 'center').subscribe();
              });

            }
          }
        ]
      });
      alert.present();
    } else { // for partial payment
      let alert = this.alertCtrl.create({
        message: 'Recieved:',
        inputs: [
          {
            name: 'Dollar',
            placeholder: 'Dollar',
            type: 'number'
          },
          {
            name: 'KHRiel',
            placeholder: 'KHRiel',
            type: 'number'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Save!',
            handler: data => {
              // Calculate recieved cash
              data.KHRiel = data.KHRiel/1000; // since we use k instead of 1000
              Partial_Recieved = Number(Partial_Recieved==null?0:Partial_Recieved) + Number(data.Dollar==null?0:data.Dollar) +
              (Number(data.KHRiel==null?0:data.KHRiel)/this.Exchange_Rate);

              // Over paid detection
              if(Partial_Recieved>Cash_Due){
                this.toast.show('OVER PAID, PAYMENT REJECTED', '15000', 'center').subscribe();
                this.get_data(null);
                this.navCtrl.parent.select(2);
                return;
              } else if(Partial_Recieved==Cash_Due){
                Is_In_Full = true;
              }
              // Query to update invoice
              this.sqlite.create({
                name: 'data.db',
                location: 'default'
              })
              .then((db: SQLiteObject) => {
                db.executeSql('UPDATE Invoices SET Cash_Paid = ?, Is_Paid = ? WHERE Invoice_ID = ?', [
                  Partial_Recieved,
                  false,
                  Invoice_ID
                ]).then(res => {
                  this.toast.show('Partial Sucessful!', '5000', 'center').subscribe();
                  this.get_data(null);
                  this.navCtrl.parent.select(2);
                })
                .catch(e => {
                  this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
                });


              }).catch(e => {
                this.toast.show(e, '5000', 'center').subscribe();
              });

            }
          }
        ]
      });
      alert.present();
    }
  }
}
