import { Component } from '@angular/core';
import { NavController, PopoverController, LoadingController } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AlertController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { SettingsPage } from '../settings/settings';
import { PropertyFormPage } from '../property-form/property-form';
import { PropertyShowPage } from '../property-show/property-show';

@Component({
  selector: 'page-property',
  templateUrl: 'property.html'
})
export class PropertyPage {
  Property: any = [];
  Selected_Propery = null;
  Month = ['NA', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
  Current_Month = this.Month[(new Date().getMonth()+1)];
  Exchange_Rate: number;

  constructor(
    public navCtrl: NavController,
    public popoverCtrl: PopoverController,
    private nativeStorage: NativeStorage,
    private sqlite: SQLite,
    private alertCtrl: AlertController,
    private toast: Toast,
    private sqlitePorter: SQLitePorter,
    private file: File,
    private socialSharing: SocialSharing,
    private fileChooser: FileChooser,
    private filePath: FilePath,
    public loadingCtrl: LoadingController
  ) {
  }

  ionViewWillEnter() {
    console.log('Perperty ionViewWillEnter');
    this.initialize();
    this.get_data(null);
  }

  import(){
    console.log('Importing Backup');
    // this.file.externalRootDirectory+'/Rent Management', 'Back_Up_Oct.json'
    this.fileChooser.open()
    .then(uri => {
      console.log('Got URI');
      this.filePath.resolveNativePath(uri)
      .then(resolvedFilePath => {
        console.log('Resolved Path')
        let path = resolvedFilePath.substring(0, resolvedFilePath.lastIndexOf('/'));
        let file = resolvedFilePath.substring(resolvedFilePath.lastIndexOf('/')+1, resolvedFilePath.length);
        console.log(path, file , 'Reading as text')
        this.file.readAsText(path,file)
        .then(file => {
          // Connect to DB
          console.log('Opening DB')
          this.sqlite.create({
            name: 'data.db',
            location: 'default'
          })
          .then((db: SQLiteObject) => {
            console.log('DB connected')
            // export DB
            this.sqlitePorter.importJsonToDb(db, file)
            .then(res => {
              this.toast.show('Restored', '5000', 'center').subscribe();
              this.get_data(null);
            })
            .catch(e => this.toast.show(JSON.stringify(e), '5000', 'center').subscribe())

          })
          .catch(e => {console.log('DB Error')});
        })
        .catch(e => console.log('Error opening file', e));

      }).catch(err => {
        console.log('Resolve Native Path',err);
      });
    })
    .catch(e => console.log(JSON.stringify(e)));
  }

  export(){
    console.log('Exporting Backup');
    // Connect to DB
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      loading.present();
      // export DB
      this.sqlitePorter.exportDbToJson(db)
      .then(json => {
        // Write JSON to File
        this.file.writeFile(this.file.externalRootDirectory+'/Rent Management', 'Back_Up_'+ this.Current_Month + '.json', json, {replace:true})
        .then(_ => {
          loading.dismissAll();
          // Alert user
          this.toast.show('Back Up Saved in Phone/Rental Management/Backup', '5000', 'center').subscribe();
          // Share to any supported platform
          this.socialSharing.share(
            'This is a Rental Management Back-up',
            'Back_Up_' + this.Current_Month,
            this.file.externalRootDirectory + '/Rent Management/Back_Up_'+ this.Current_Month + '.json');
          })
          .catch(err => {
            loading.dismissAll();
            this.toast.show('After Granting Storage Access, Please Export Again', '5000', 'center').subscribe()
          });
        })
        .catch(e => console.error(JSON.stringify(e)));

      })
      .catch(e => console.log(JSON.stringify(e)));
    }

    initialize(){
      console.log('Initializing');
      // create a directory in Root of Phone public Storage if not exist
      this.file.createDir(this.file.externalRootDirectory, 'Rent Management', false)
      .then(_ => console.log('Dir Created'))
      .catch(err => {
        console.log('Dir alr created');
      });

      this.nativeStorage.getItem('Exchange_Rate')
      .then(
        data => console.log("Exchange Rate Exist, Db alr initialized"),
        error => {
          this.nativeStorage.setItem('Exchange_Rate', {USD_KHR: 4000})
          .then( _ => {
            console.log('Exchange Rate inserted');
            this.sqlite.create({
              name: 'data.db',
              location: 'default'
            })
            .then((db: SQLiteObject) => {
              db.sqlBatch([
                'CREATE TABLE IF NOT EXISTS Property(Property_ID INTEGER PRIMARY KEY AUTOINCREMENT,Name VARCHAR(20) UNIQUE,Is_Active BOOLEAN,Description VARCHAR(100),Electric_Price INT(5),Water_Price INT(5));',

                'CREATE TABLE IF NOT EXISTS Rooms(Room_ID INTEGER PRIMARY KEY AUTOINCREMENT,Room_Number VARCHAR(5), Is_Active BOOLEAN,Owner VARCHAR(20),FK_Property_ID INT(2),Date_In DATE,Price INT(7),Electric_Meter INT(6),Water_Meter INT(6),FOREIGN KEY(FK_Property_ID) REFERENCES Property(Property_ID) ON DELETE CASCADE);',

                'CREATE TABLE IF NOT EXISTS Invoices(Invoice_ID INTEGER PRIMARY KEY AUTOINCREMENT,FK_Room_ID VARCHAR(5),Type VARCHAR(5),Begin_Date DATE,End_Date DATE,Begin_Electric_Meter INT(6),End_Electric_Meter INT(6),Begin_Water_Meter INT(6),End_Water_Meter INT(6),Cash_Due VARCHAR(12),Cash_Paid VARCHAR(12),Is_Paid BOOLEAN, FOREIGN KEY(FK_Room_ID) REFERENCES Rooms(Room_ID) ON DELETE CASCADE);',

                'CREATE TABLE IF NOT EXISTS TempUtil(TempUtil_ID INTEGER PRIMARY KEY AUTOINCREMENT, Room_ID INTEGER UNIQUE, Room_Number VARCHAR(5), User_Leave BOOLEAN, Price INTEGER, Date DATE, Electric_Meter INT(6),Water_Meter INT(6));'
              ])
              .then(() => {
                console.log('DB Initialized');
                this.get_data(null);
              })
              .catch(e => console.log(JSON.stringify(e)));

            })
            .catch(e => console.log(JSON.stringify(e)));
          },
          error => console.error('Error storing item', error)
        );
      }
    );
  }

  edit_exchange_rate(){
    let alert = this.alertCtrl.create({
      title: 'New Exchange Rate',
      inputs: [
        {
          name: 'Exchange_Rate',
          placeholder: 'Exchange Rate'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Save!',
          handler: data => {
            this.nativeStorage.setItem('Exchange_Rate',{USD_KHR: data.Exchange_Rate})
            .then(
              () => {
                this.toast.show('Exchange Rate Updated', '5000', 'center').subscribe();
                this.get_data(null);
              },
              error => {}
            );
          }
        }
      ]
    });
    alert.present();
  }
  select(p_id, name){
    console.log('Selecting Property');
    this.Selected_Propery = p_id;
    this.nativeStorage.setItem('Selected_Property',{Property_ID: p_id, Name: name})
    .then(
      () => {
        this.navCtrl.parent.select(1);
      },
      error => {}
    );
  }

  add_property(){
    console.log('Getting Property Form');
    this.navCtrl.push(PropertyFormPage);
  }

  get_data(refresher){
    console.log('Getting Property Data');
    console.log('Getting Selected Property');
    this.nativeStorage.getItem('Selected_Property')
    .then(
      data => {
        this.Selected_Propery = data.Property_ID;
      },
      error => {}
    );
    console.log('Getting Exchange Rate');
    this.nativeStorage.getItem('Exchange_Rate')
    .then(
      data => {
        this.Exchange_Rate = data.USD_KHR;
      },
      error => {}
    );
    console.log('Getting Property From DB');
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {

      db.executeSql('SELECT * FROM Property', [])
      .then(res => {
        console.log('Pushing Property Data');
        this.Property = [];
        for(var i=0; i<res.rows.length; i++) {
          this.Property.push(res.rows.item(i));
        }
        if(refresher) refresher.complete();
      })
      .catch(e => console.log(JSON.stringify(e)));

    })
    .catch(e => console.log(JSON.stringify(e)));
  }

  show_property(p_id){
    console.log('Showing Note');
    this.navCtrl.push(PropertyShowPage, {Property_ID:p_id});
  }

  change_status(current_status, p_id) {
    console.log('Change Property Active Status');
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {

      db.executeSql('UPDATE Property SET Is_Active = ? WHERE Property_ID = ?', [current_status == 'true' ? false: true, p_id])
      .then(() => console.log('Property Status Updated'))
      .catch(e => console.log(JSON.stringify(e)));
      this.get_data(null);

    })
    .catch(e => console.log(JSON.stringify(e)));
  }

  // pop trip overview list
  present_settings(p_id) {
    console.log('Property Settings');
    const popover = this.popoverCtrl.create(
      SettingsPage,
      {Property_ID: p_id}
    );
    popover.onDidDismiss( data =>{
      this.get_data(null);
    });
    popover.present();
  }

  delete_property(p_id){
    let alert = this.alertCtrl.create({
      title: 'Deleting this property will also delete all associated rooms and invoices!',
      inputs: [
        {
          name: 'Password',
          placeholder: 'Password'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'DELETE',
          handler: data => {
            if(data.Password == 'pokpok') {
              this.sqlite.create({
                name: 'data.db',
                location: 'default'
              }).then((db: SQLiteObject) => {
                db.executeSql('SELECT Room_ID FROM Property WHERE FK_Property_ID = ?;', [p_id])
                .then(res => {
                  for(var i=0; i<res.rows.length; i++) {
                    db.executeSql('DELETE FROM Invoices WHERE FK_Room_ID = ?;', [res.rows.item(i).Room_ID])
                    .then(res => {
                      this.toast.show('Invoices Deleted', '5000', 'center').subscribe();
                    })
                    .catch(e => console.log(JSON.stringify(e)));
                  }
                })
                .catch(e => console.log(JSON.stringify(e)));
                db.executeSql('DELETE FROM Rooms WHERE FK_Property_ID = ?;', [p_id])
                .then(res => {
                  this.toast.show('Rooms Deleted', '5000', 'center').subscribe();
                })
                .catch(e => console.log(JSON.stringify(e)));
                db.executeSql('DELETE FROM Property WHERE Property_ID = ?;', [p_id])
                .then(res => {
                  this.toast.show('Property Deleted', '5000', 'center').subscribe();
                  this.get_data(null);
                })
                .catch(e => console.log(JSON.stringify(e)));
              })
              .catch(e => console.log(JSON.stringify(e)));
            }
          }
        }
      ]
    });
    alert.present();
  }

}
