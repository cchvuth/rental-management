// Show Property Note
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

/**
 * Generated class for the PropertyShowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-property-show',
  templateUrl: 'property-show.html',
})
export class PropertyShowPage {
  Property: any = {
    Property_ID: 0,
    Name: '',
    Description: ''
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
  ) {
    this.get_property(this.navParams.data.Property_ID);
  }

  get_property(p_id){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('SELECT Property_ID, Name, Description FROM Property WHERE Property_ID = ?', [p_id])
        .then(res => {
            this.Property = res.rows.item(0);
        })
        .catch(e => console.log(e));

      })
      .catch(e => console.log(e));
  }

  save_description(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('UPDATE Property SET Description = ? WHERE Property_ID = ?', [
          this.Property.Description,
          this.Property.Property_ID
        ]).then(res => {
          this.toast.show('Note Saved', '5000', 'center').subscribe();
          this.navCtrl.parent.select(0);
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
        });

      }).catch(e => {
      this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
    });
  }

}
