import { Component} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the RoomEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-room-edit',
  templateUrl: 'room-edit.html',
})
export class RoomEditPage {
  Rooms:any = {
    Room_ID: null,
    Room_Number: '',
    Owner: '',
    Price: 0
    // Electric_Meter: 0,
    // Water_Meter: 0
  }
  Meter = {
    Meter_Is_Replaced: false,
    Input_Date: '',
    Old_Electric_Meter_Stuck: null,
    Old_Water_Meter_Stuck: null,
    New_Electric_Meter_Was: null,
    New_Water_Meter_Was: null,
    New_Electric_Meter_Now: null,
    New_Water_Meter_Now: null
  }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    private nativeStorage: NativeStorage
  ) {
  }

  ionViewWillLoad() {
    this.get_current_settings(this.navParams.data.Room_ID);
  }

  get_current_settings(r_id){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('SELECT Room_ID, Room_Number, Owner, Price FROM Rooms WHERE Room_ID = ?', [r_id])
        .then(res => {
          this.Rooms.Room_ID = res.rows.item(0).Room_ID;
          this.Rooms.Room_Number = res.rows.item(0).Room_Number;
          this.Rooms.Owner = res.rows.item(0).Owner;
          this.Rooms.Price = res.rows.item(0).Price;
          // this.Rooms.Electric_Meter = res.rows.item(0).Electric_Meter;
          // this.Rooms.Water_Meter = res.rows.item(0).Water_Meter;
        })
        .catch(e => console.log(JSON.stringify(e)));

      })
      .catch(e => console.log(JSON.stringify(e)));
  }

  get_reset_form(){
    this.Meter.Meter_Is_Replaced = true;
  }

  create_meter_reset_invoice(){
    let Exchange_Rate;
    this.nativeStorage.getItem('Exchange_Rate')
    .then(
      data => {
        Exchange_Rate = Number(data.USD_KHR);
      },
      error => console.error(error)
    );
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        // select property prices;
        db.executeSql('SELECT Rooms.Price, Invoices.Invoice_ID, Invoices.End_Date, Invoices.End_Electric_Meter, Invoices.End_Water_Meter, Property.Electric_Price, Property.Water_Price FROM Invoices INNER JOIN Rooms ON Invoices.FK_Room_ID = Rooms.Room_ID INNER JOIN Property ON Rooms.FK_Property_ID = Property_ID WHERE Rooms.Room_ID = ? AND Invoices.Invoice_ID = (SELECT Invoice_ID FROM Invoices WHERE Invoices.FK_Room_ID = Rooms.Room_ID ORDER BY Invoice_ID DESC LIMIT 1) LIMIT 1', [this.Rooms.Room_ID])
        .then(res => {
          var Electric_Usage = Number(this.Meter.Old_Electric_Meter_Stuck - res.rows.item(0).End_Electric_Meter) + Number(this.Meter.New_Electric_Meter_Now - this.Meter.New_Electric_Meter_Was);
          var Electric_Cost = Electric_Usage * Number(res.rows.item(0).Electric_Price);
          var Water_Usage = Number(this.Meter.Old_Water_Meter_Stuck - res.rows.item(0).End_Water_Meter) + Number(this.Meter.New_Water_Meter_Now - this.Meter.New_Water_Meter_Was);
          var Water_Cost = Water_Usage * Number(res.rows.item(0).Water_Price);
          var Total_USD = (Electric_Cost + Water_Cost)/Exchange_Rate;

          db.executeSql('INSERT INTO Invoices (FK_Room_ID, Type, Begin_Date, End_Date, Begin_Electric_Meter, End_Electric_Meter, Begin_Water_Meter, End_Water_Meter, Cash_Due, Cash_Paid, Is_Paid) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [
            this.Rooms.Room_ID,
            "RESET",
            res.rows.item(0).End_Date,
            this.Meter.Input_Date,
            this.Meter.Old_Electric_Meter_Stuck,
            this.Meter.New_Electric_Meter_Now,
            this.Meter.Old_Water_Meter_Stuck,
            this.Meter.New_Water_Meter_Now,
            Number(res.rows.item(0).Price) + Total_USD,
            0,
            false
          ])
          .then(res => {
            this.toast.show('Reset Invoice Created', '5000', 'center').subscribe();
            db.executeSql('UPDATE Rooms SET Electric_Meter = ?, Water_Meter = ? WHERE Room_ID = ?;', [
              this.Meter.New_Electric_Meter_Now,
              this.Meter.New_Water_Meter_Now,
              this.Rooms.Room_ID
            ])
            .then(res => {
              this.toast.show('Room Meters Updated', '5000', 'center').subscribe();
              this.navCtrl.parent.select(1);
            })
            .catch(e => console.log(JSON.stringify(e)));
          })
          .catch(e => console.log(JSON.stringify(e)));
        })
        .catch(e => console.log(JSON.stringify(e)));

      })
      .catch(e => console.log(JSON.stringify(e)));
  }

  save_room(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('UPDATE Rooms SET Room_Number = ?, Owner = ?, Price = ? WHERE Room_ID = ?', [
          this.Rooms.Room_Number,
          this.Rooms.Owner,
          this.Rooms.Price,
          this.Rooms.Room_ID
        ]).then(res => {
          this.toast.show('Settings Saved', '5000', 'center').subscribe();
          this.navCtrl.parent.select(1);
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
        });

      }).catch(e => {
      this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
    });
  }

}
