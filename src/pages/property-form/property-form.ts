// for adding property
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';


/**
 * Generated class for the PropertyFormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-property-form',
  templateUrl: 'property-form.html',
})
export class PropertyFormPage {

  Property: any = {
    Name : '',
    Is_Active : false,
    Description : '',
    Electric_Price: null,
    Water_Price: null
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
  ) {
  }

  add_property(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql(
          'INSERT INTO Property (Name, Is_Active, Description, Electric_Price, Water_Price) VALUES(?,?,?,?,?)',
           [
             this.Property.Name,
             this.Property.Is_Active,
             this.Property.Description,
             this.Property.Electric_Price,
             this.Property.Water_Price
           ]
         ).then(res => {
          this.toast.show('Property Added', '5000', 'center').subscribe();
          this.navCtrl.parent.select(0);
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
        });

      })
      .catch(e => console.log(JSON.stringify(e)));
  }

}
