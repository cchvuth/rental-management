import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { NativeStorage } from '@ionic-native/native-storage';
import { Toast } from '@ionic-native/toast';

/**
* Generated class for the InvoiceFormPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@Component({
  selector: 'page-invoice-form',
  templateUrl: 'invoice-form.html',
})
export class InvoiceFormPage {
  Inputs:any = {
    Room_ID: null,
    Room_Number: 'Num',
    User_Leave: false,
    Price: null,
    Date: new Date().toISOString(),
    Electric_Meter:null,
    Water_Meter:null
  }

  Temp_Util_Exist: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    private nativeStorage: NativeStorage,
  ) {
    this.Inputs.Room_ID = this.navParams.data.Room_ID;
    this.Inputs.Room_Number = this.navParams.data.Room_Number;
    this.Inputs.Price = this.navParams.data.Price;
    this.Inputs.User_Leave = this.navParams.data.User_Leave
    this.get_util();
  }

  get_util(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM TempUtil WHERE Room_ID = ?;', [this.Inputs.Room_ID])
      .then(res => {
        if (res.rows.item(1)) {
          this.toast.show('Duplicate Save Detected', '5000', 'center').subscribe();
        }
        if(res.rows.item(0) != null) {
          this.Temp_Util_Exist = true;
          this.Inputs = res.rows.item(0);
        }
      })
      .catch(e => console.log(e));
    })
  }

  save_util(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      if (this.Temp_Util_Exist){
        db.executeSql('DELETE FROM TempUtil WHERE Room_ID = ?;', [this.Inputs.Room_ID])
        .catch(e => console.log(e));
      }

      db.executeSql(
        'INSERT INTO TempUtil (Room_ID, Room_Number, User_Leave, Price, Date, Electric_Meter, Water_Meter) VALUES (?,?,?,?,?,?,?);',
        [
          this.Inputs.Room_ID,
          this.Inputs.Room_Number,
          this.Inputs.User_Leave,
          this.Inputs.Price,
          this.Inputs.Date,
          this.Inputs.Electric_Meter,
          this.Inputs.Water_Meter
        ]
      )
      .then(res => {
        this.toast.show( 'Saved!', '5000', 'center').subscribe();
        this.navCtrl.parent.select(1);
      })
      .catch(e => console.log(e));
    })
  }

  add_invoice(){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {

      db.executeSql('DELETE FROM TempUtil WHERE Room_ID = ?;', [this.Inputs.Room_ID])
      .then(res => {
        this.Temp_Util_Exist = true;
      })
      .catch(e => console.log(e));

      // DECLARE VARIABLES
      var Rooms = {
        Is_Active: null,
        Price: null,
        Electric_Meter: null,
        Water_Meter: null
      }, Invoices = {
        Type: null,
        Begin_Date: null,
        End_Date: null,
        Begin_Electric_Meter: null,
        End_Electric_Meter: null,
        Begin_Water_Meter: null,
        End_Water_Meter: null,
      }, Property = {
        Electric_Price: null,
        Water_Price: null
      }; // Room Status, Prev Month Invvoice Type
      // GET EXCHANGE RATE
      var Exchange_Rate;
      this.nativeStorage.getItem('Exchange_Rate')
      .then(
        data => {
          Exchange_Rate = data.USD_KHR;
        },
        error => console.error(error)
      );
      // QUERY DATA
      db.executeSql('SELECT Invoices.Invoice_ID, Invoices.Type, Invoices.Begin_Date, Invoices.End_Date, Invoices.Begin_Electric_Meter, Invoices.End_Electric_Meter, Invoices.Begin_Water_Meter, Invoices.End_Water_Meter, Rooms.Is_Active, Rooms.Price, Rooms.FK_Property_ID, Rooms.Electric_Meter, Rooms.Water_Meter, Property.Property_ID, Property.Electric_Price, Property.Water_Price FROM Invoices INNER JOIN Rooms ON Rooms.Room_ID = Invoices.FK_Room_ID INNER JOIN Property ON Rooms.FK_Property_ID = Property.Property_ID WHERE FK_Room_ID = ? ORDER BY Invoice_ID DESC LIMIT 1;', [this.Inputs.Room_ID])
      .then(async res => {
        // ASSINING DATA
        if(res.rows.length != 0){
          Rooms.Is_Active = res.rows.item(0).Is_Active;
          Rooms.Price = res.rows.item(0).Price;
          Rooms.Electric_Meter = res.rows.item(0).Electric_Meter;
          Rooms.Water_Meter = res.rows.item(0).Water_Meter;
          Invoices.Type = res.rows.item(0).Type;
          Invoices.Begin_Date = res.rows.item(0).Begin_Date;
          Invoices.End_Date = res.rows.item(0).End_Date;
          Invoices.Begin_Electric_Meter = res.rows.item(0).Begin_Electric_Meter;
          Invoices.End_Electric_Meter = res.rows.item(0).End_Electric_Meter;
          Invoices.Begin_Water_Meter = res.rows.item(0).Begin_Water_Meter;
          Invoices.End_Water_Meter = res.rows.item(0).End_Water_Meter;
          Property.Electric_Price = res.rows.item(0).Electric_Price;
          Property.Water_Price = res.rows.item(0).Water_Price;
        }
        if((!Rooms.Is_Active || Rooms.Is_Active =='false') && (Invoices.Type == 'LEAVE' || Invoices.Type == null)){
          // New User Invoice
          db.executeSql('INSERT INTO Invoices (FK_Room_ID, Type, Begin_Date, End_Date, Begin_Electric_Meter, End_Electric_Meter, Begin_Water_Meter, End_Water_Meter, Cash_Due, Cash_Paid, Is_Paid) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [
            this.Inputs.Room_ID,
            'BOOK',
            this.Inputs.Date,
            this.Inputs.Date,
            this.Inputs.Electric_Meter,
            this.Inputs.Electric_Meter,
            this.Inputs.Water_Meter,
            this.Inputs.Water_Meter,
            this.Inputs.Price+(35000/Exchange_Rate),
            0,
            false
          ])
          .then(res => {
            this.toast.show('Booking Invoice Generated', '5000', 'center').subscribe();
          })
          .catch(e => console.log(JSON.stringify(e)));

        } else if(Rooms.Is_Active && Invoices.Type == 'BOOK'){
          // END OF FIRST MONTH AFTER BOOK
          let days_elapsed;
          let old_day = new Date(Invoices.Begin_Date).getDate();
          let new_day = new Date(this.Inputs.Date).getDate();
          let old_month = new Date(Invoices.Begin_Date).getMonth()+1;
          let new_month = new Date(this.Inputs.Date).getMonth()+1;
          if(old_month == new_month) days_elapsed = new_day - old_day;

          else if(new_month > old_month) days_elapsed = (30*(new_month-old_month)-old_day+new_day);
          else days_elapsed = 360+(30*(new_month-old_month)-old_day+new_day)

          let Room_Due = ((this.Inputs.Price/30) * days_elapsed) +
          ((
            (
              (
                this.Inputs.Electric_Meter - Rooms.Electric_Meter
              ) * Property.Electric_Price
            ) + (
              (
                this.Inputs.Water_Meter - Rooms.Water_Meter
              )* Property.Water_Price
            )
          )/Exchange_Rate)
          db.executeSql('INSERT INTO Invoices (FK_Room_ID, Type, Begin_Date, End_Date, Begin_Electric_Meter, End_Electric_Meter, Begin_Water_Meter, End_Water_Meter, Cash_Due, Cash_Paid, Is_Paid) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [
            this.Inputs.Room_ID,
            'FIRST',
            Invoices.Begin_Date,
            this.Inputs.Date,
            Invoices.Begin_Electric_Meter,
            this.Inputs.Electric_Meter,
            Invoices.Begin_Water_Meter,
            this.Inputs.Water_Meter,
            Room_Due,
            0,
            false
          ])
          .then(res => {
            this.toast.show('Invoice Generated', '5000', 'center').subscribe();
          })
          .catch(e => console.log(JSON.stringify(e)));

        } else {
          // NORMAL OR lEAVE INVOICE
          let days_elapsed;
          if(this.Inputs.User_Leave){
            let old_day = new Date(Invoices.End_Date).getDate();
            let new_day = new Date(this.Inputs.Date).getDate();
            let old_month = new Date(Invoices.End_Date).getMonth()+1;
            let new_month = new Date(this.Inputs.Date).getMonth()+1;
            console.log('OD', old_day, 'ND', new_day, 'OM', old_month, 'NM', new_month)
            if(old_month == new_month) days_elapsed = new_day - old_day;
            else if(new_month>old_month) days_elapsed = (30*(new_month-old_month)-old_day+new_day);
            else days_elapsed = 360+(30*(new_month-old_month)-old_day+new_day)
          }
          console.log('Days ELAPSED', days_elapsed);

          let Room_Due =
          ((
            (
              (
                this.Inputs.Electric_Meter - Invoices.End_Electric_Meter
              ) * Property.Electric_Price
            ) + (
              (
                this.Inputs.Water_Meter - Invoices.End_Water_Meter
              )* Property.Water_Price
            )
          )/Exchange_Rate)+(this.Inputs.User_Leave?((this.Inputs.Price/30)*days_elapsed):this.Inputs.Price);
          console.log('Room Due', Room_Due);
          db.executeSql('INSERT INTO Invoices (FK_Room_ID, Type, Begin_Date, End_Date, Begin_Electric_Meter, End_Electric_Meter, Begin_Water_Meter, End_Water_Meter, Cash_Due, Cash_Paid, Is_Paid) VALUES (?,?,?,?,?,?,?,?,?,?,?)', [
            this.Inputs.Room_ID,
            this.Inputs.User_Leave ? 'LEAVE' : 'NORM',
            Invoices.End_Date,
            this.Inputs.Date,
            Invoices.End_Electric_Meter,
            this.Inputs.Electric_Meter,
            Invoices.End_Water_Meter,
            this.Inputs.Water_Meter,
            Room_Due,
            0,
            false
          ])
          .then(res => {
            this.toast.show((this.Inputs.User_Leave ? 'Payback Invoice Generated' : '' + 'Invoice Generated'), '5000', 'center').subscribe();
          })
          .catch(e => console.log(JSON.stringify(e)));
        }

        // UPDATE ROOM METERS AFTER CREATING INVOICE

        db.executeSql('UPDATE Rooms SET Is_Active = ?, Electric_Meter = ?, Water_Meter = ? WHERE Room_ID = ?;', [
          this.Inputs.User_Leave?false:true,
          this.Inputs.Electric_Meter,
          this.Inputs.Water_Meter,
          this.Inputs.Room_ID
        ])
        .then(res => {
          this.navCtrl.parent.select(1);
        })
        .catch(e => console.log(JSON.stringify(e)));

      })
      .catch(e => console.log(JSON.stringify(e)));

    })
    .catch(e => console.log(JSON.stringify(e)));
  }

}
