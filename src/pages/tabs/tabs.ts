import { Component } from '@angular/core';

import { InputPage } from '../input/input';
import { CollectPage } from '../collect/collect';
import { PropertyPage } from '../property/property';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = PropertyPage;
  tab2Root = InputPage;
  tab3Root = CollectPage;

  constructor() {

  }
}
