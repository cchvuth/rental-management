// Input/Invoice Creation Tab
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { NativeStorage } from '@ionic-native/native-storage';
import { Toast } from '@ionic-native/toast';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import {get_imgs} from './img_dataurl';

import { RoomFormPage } from '../room-form/room-form';
import { RoomShowPage } from '../room-show/room-show';
import { RoomEditPage } from '../room-edit/room-edit';
import { InvoiceFormPage } from '../invoice-form/invoice-form';

@Component({
  selector: 'page-input',
  templateUrl: 'input.html'
})
export class InputPage {

  Data:any = [];
  Prices: any = {};
  Exchange_Rate: number;
  Property_ID: number;
  Property_Name: string;
  Current_Month = new Date().getMonth()+1;
  Input_Remain:number = 0; // input remain this month
  Month = ['NA', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

  // For making PDF
  letterObj = {
    to: '',
    from: '',
    text: ''
  }
  pdfObj = null;

  constructor(
    public navCtrl: NavController,
    private sqlite: SQLite,
    private nativeStorage: NativeStorage,
    private toast: Toast,
    private file: File,
    private socialSharing: SocialSharing,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {

  }

  ionViewWillEnter() {
    this.Input_Remain = 0; // Input needed for current month
    this.get_data(null);
  }

  add_rooms(){
    this.navCtrl.push(RoomFormPage);
  }

  view_room(r_id){
    this.navCtrl.push(RoomShowPage, {Room_ID: r_id});
  }

  edit_room(r_id){
    this.navCtrl.push(RoomEditPage, {Room_ID: r_id});
  }

  add_invoice(r_id, r_num, price){
    this.navCtrl.push(InvoiceFormPage, {Room_ID: r_id, Room_Number: r_num, Price: price});
  }

  user_leave(r_id, price){
    this.navCtrl.push(InvoiceFormPage, {User_Leave: true, Room_ID: r_id, Price: price});
  }

  toast_remain() {
    this.toast.show('Input remained to be noted for this month: '+ this.Input_Remain, '5000', 'center').subscribe();
  }

  create_pdf() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    var imgs = get_imgs();
    var content = [];
    this.nativeStorage.getItem('Exchange_Rate')
    .then(
      data => {
        this.Exchange_Rate = data.USD_KHR;
      },
      error => console.error(error)
    );
    this.Data.forEach(data => {
      if(data.Is_Active == 'false') return;
      data.Cash_Due = Number(data.Cash_Due);
      let electric_total_price = Number((data.End_Electric_Meter-data.Begin_Electric_Meter)*this.Prices.Electric_Price);
      let water_total_price = Number((data.End_Water_Meter-data.Begin_Water_Meter)*this.Prices.Water_Price);
      content.push(
        {
          image: imgs.invoice,
          fit: [100, 100],
          alignment:'center'
        },
        {
          columns: [
            {
              image: imgs.room,
              width: 50,
            },
            {text: data.Room_Number, style: 'header'},
          ]
        },
        {
          columns: [
            {
              image: imgs.invoice_date,
              width: 170,
            },
            {text: data.End_Date, style: 'header'},
          ]
        },
        {
          columns: [
            {
              image: imgs.calculated_from,
              width: 120,
            },
            {text: data.Begin_Date, style: 'header'},
            {
              image: imgs.till,
              width: 80,
            },
            {text: data.End_Date, style: 'header'},
          ]
        },
        {
          style: 'tableExample',
          table: {
            widths: [37, 57, 77,85, 68, 40, '*'],
            body: [
              [
                {text: '', style: 'tableHeader'},
                {image: imgs.new_meter,width: 60},
                {image: imgs.old_meter,width: 80},
                {image: imgs.usage,width: 90},
                {image: imgs.unit_cost,width: 70},
                {image: imgs.total,width: 30},
                {image: imgs.total_riel,width: 80},
              ],
              [
                {image: imgs.electric,width: 35},
                {text: data.End_Electric_Meter, style: 'header'},
                {text: data.Begin_Electric_Meter, style: 'header'} ,
                {text:data.End_Electric_Meter-data.Begin_Electric_Meter, style: 'header'} ,
                {
                  columns: [
                    {text: this.Prices.Electric_Price, style: 'header'},
                    {
                      image: imgs.riel,
                      width: 7,
                    }
                  ]
                }, '',
                {
                  columns: [
                    {text: electric_total_price, style: 'header'},
                    {
                      image: imgs.riel,
                      width: 7,
                    }
                  ]
                }
              ],
              [
                {image: imgs.water,width: 35},
                {text: data.End_Water_Meter, style: 'header'} ,
                {text: data.Begin_Water_Meter, style: 'header'} ,
                {text: data.End_Water_Meter-data.Begin_Water_Meter, style: 'header'} ,
                {
                  columns: [
                    {text:this.Prices.Water_Price, style: 'header'},
                    {
                      image: imgs.riel,
                      width: 7,
                    }
                  ]
                },'',
                {
                  columns: [
                    {text:water_total_price, style: 'header'},
                    {
                      image: imgs.riel,
                      width: 7,
                    }
                  ]
                }
              ],
              [
                {image: imgs.room,width: 40, colSpan:5, alignment:'center'},
                '', '','', '',
                {text: ((data.Cash_Due < data.Price) ? (data.Cash_Due-((electric_total_price+water_total_price)/this.Exchange_Rate)) : (data.Price)) + '$', style: 'header'},''
              ],					[
                {image: imgs.total,width: 45, colSpan:5, alignment:'right'},
                '', '','', '',
                {text: (data.Cash_Due < data.Price) ? (data.Cash_Due-((electric_total_price+water_total_price)/this.Exchange_Rate)) : (data.Price) + '$', style: 'header'},
                {
                  columns: [
                    {text: (data.Cash_Due < data.Price) ? (electric_total_price+water_total_price) : (electric_total_price+water_total_price), style: 'header'},
                    {
                      image: imgs.riel,
                      width: 7,
                    }
                  ]
                }
              ],
              //colSpan: 2, rowSpan:2
            ]
          },

        },
        {image: imgs.complaint,width: 510, pageBreak:'after'},
      )
    })
    var docDefinition = {
      content: content,
      styles: {
        header: {
          fontSize: 22,
          bold: true,
          margin: [0, 0, 0, 10]
        },
        subheader: {
          fontSize: 20,
          bold: true,
          margin: [0, 10, 0, 5]
        },
        tableExample: {
          margin: [0, 5, 0, 15]
        },
        tableHeader: {
          bold: true,
          fontSize: 20,
          color: 'black'
        }
      },
      defaultStyle: {
        alignment: 'justify'
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
    this.pdfObj.getBuffer((buffer) => {
      var blob = new Blob([buffer], { type: 'application/pdf' });

      // Save the PDF to the data Directory of our App
      this.file.writeFile(this.file.externalRootDirectory+'/Rent Management', 'Invoice.pdf', blob, { replace: true }).then(fileEntry => {
        loading.dismiss();
        this.toast.show('PDF Generated at /Rental Management/Invoice.pdf', '5000', 'center').subscribe();
        // Share to any supported platform
        this.socialSharing.share(
          'This is a Rental Management Back-up',
          'Invoice.pdf',
          this.file.externalRootDirectory + '/Rent Management/Invoice.pdf');
        })
      });
    }

    get_data(refresher){
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      // get selected property ID
      this.nativeStorage.getItem('Selected_Property')
      .then(
        data => {
          this.Property_ID = data.Property_ID;
          this.Property_Name = data.Name;

          this.sqlite.create({
            name: 'data.db',
            location: 'default'
          }).then((db: SQLiteObject) => {
            db.executeSql('SELECT Electric_Price, Water_Price FROM Property WHERE Property_ID = ?;', [this.Property_ID])
            .then(res => {
              this.Prices = res.rows.item(0);
            })
            .catch(e => console.log(JSON.stringify(e)));


            db.executeSql('SELECT Rooms.Room_ID, Rooms.Room_Number, Rooms.Is_Active, Rooms.Price, strftime(\'%d/%m/%Y\', Invoices.Begin_Date) AS Begin_Date, strftime(\'%d/%m/%Y\', Invoices.End_Date) AS End_Date , Invoices.Begin_Electric_Meter, Invoices.End_Electric_Meter, Invoices.Begin_Water_Meter, Invoices.End_Water_Meter, Invoices.Cash_Due FROM Rooms LEFT JOIN Invoices ON Invoices.Invoice_ID = (SELECT Invoice_ID FROM Invoices WHERE Invoices.FK_Room_ID = Rooms.Room_ID ORDER BY Invoice_ID DESC LIMIT 1) WHERE Rooms.FK_Property_ID = ?;', [this.Property_ID])
            .then(res => {
              this.Data = [];
              this.Input_Remain = res.rows.length;
              for(var i=0; i<res.rows.length; i++) {
                this.Data.push(res.rows.item(i));
                // if end date of last invoice exist
                if(this.Data[i].End_Date){
                  // get month number
                  let temp_month = Number(this.Data[i].End_Date.substr(3,2));
                  // retrieve first 3 char from declared array
                  this.Data[i].Three_Letter_Month = this.Month[temp_month];
                  // decrement when its equal to current month
                  if(temp_month == this.Current_Month) this.Input_Remain--;
                }
              }
              loading.dismiss();
              if(refresher) refresher.complete();
            })
            .catch(e => console.log(JSON.stringify(e)));

          })
          .catch(e => console.log(JSON.stringify(e)));

        },
        error => console.error(error)
      )
      .catch(e => this.toast.show('Please select a property to begin with', '5000', 'center').subscribe())
    }

    delete_room(r_id){
      let alert = this.alertCtrl.create({
        title: 'Deleting this room will also delete all associated invoices!',
        inputs: [
          {
            name: 'Password',
            placeholder: 'Password'
          }
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'DELETE',
            handler: data => {
              if(data.Password == 'pokpok') {
                this.sqlite.create({
                  name: 'data.db',
                  location: 'default'
                }).then((db: SQLiteObject) => {
                  db.executeSql('DELETE FROM Invoices WHERE FK_Room_ID = ?;', [r_id])
                  .then(res => {
                    this.toast.show('Invoices Deleted', '5000', 'center').subscribe();
                  })
                  .catch(e => console.log(JSON.stringify(e)));
                  db.executeSql('DELETE FROM Rooms WHERE Room_ID = ?;', [r_id])
                  .then(res => {
                    this.toast.show('Room Deleted', '5000', 'center').subscribe();
                    this.get_data(null);
                  })
                  .catch(e => console.log(JSON.stringify(e)));
                })
                .catch(e => console.log(JSON.stringify(e)));
              }
            }
          }
        ]
      });
      alert.present();
    }

  }
