// Room Usage History
import { Component} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { InvoiceOverridePage } from '../invoice-override/invoice-override';

/**
 * Generated class for the RoomShowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-room-show',
  templateUrl: 'room-show.html',
})
export class RoomShowPage {
  Rooms:any = {
    Room_ID:null,
    Room_Number: '',
    Is_Active: null,
    Owner: null,
    Price: null,
    Electric_Meter: null,
    Water_Meter: null
  }
  Invoices: any = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite
  ) {
    this.get_room_invoices(this.navParams.data.Room_ID)
  }

  invoice_override(Invoice_ID){
    this.navCtrl.push(InvoiceOverridePage, {
      Invoice_ID:Invoice_ID
    })
  }

  get_room_invoices(r_id){
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('SELECT Room_ID, Room_Number, Is_Active, Owner, Price, Electric_Meter, Water_Meter FROM Rooms WHERE Room_ID = ?', [r_id])
        .then(res => {
          this.Rooms = res.rows.item(0);
        })
        .catch(e => console.log(JSON.stringify(e)));


        db.executeSql('SELECT *, strftime(\'%d/%m/%Y\', Invoices.Begin_Date) AS Begin_Date, strftime(\'%d/%m/%Y\', End_Date) AS End_Date FROM Invoices WHERE FK_Room_ID = ? ORDER BY Invoice_ID DESC LIMIT 5;', [r_id])
        .then(res => {
          this.Invoices = [];
          for(var i=0; i<res.rows.length; i++) {
            this.Invoices.push(res.rows.item(i));
          }
        })
        .catch(e => console.log(JSON.stringify(e)));

      })
      .catch(e => console.log(JSON.stringify(e)));
  }
}
