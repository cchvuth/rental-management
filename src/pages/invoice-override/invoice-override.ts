import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the InvoiceOverridePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-invoice-override',
  templateUrl: 'invoice-override.html',
})
export class InvoiceOverridePage {

  Invoice:any = {};
  Data:any = {};
  Exchange_Rate: any = 4000;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    private nativeStorage: NativeStorage,
  ) {
    this.get_current_settings(this.navParams.data.Invoice_ID);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InvoiceOverridePage');
  }

  get_current_settings(i_id){
    // connect to db

    this.nativeStorage.getItem('Exchange_Rate')
    .then(
      data => {
        this.Exchange_Rate = data.USD_KHR;
      },
      error => console.error(error)
    );

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('SELECT * FROM Invoices WHERE Invoice_ID = ?', [i_id])
        .then(res => {
          this.Invoice = res.rows.item(0);

          db.executeSql('SELECT Rooms.Room_Number, Rooms.Price, Property.Electric_Price, Property.Water_Price from Rooms INNER JOIN Property ON Rooms.FK_Property_ID = Property.Property_ID WHERE Rooms.Room_ID = ? LIMIT 1;', [this.Invoice.FK_Room_ID])
          .then(res => {
            this.Data = res.rows.item(0);
          })
          .catch(e => console.log(e));

        })
        .catch(e => console.log(e));

      })
      .catch(e => console.log(e));
  }

  save_settings(){

    if (this.Invoice.Cash_Due == '') {
        this.Invoice.Cash_Due = this.Data.Price +
        (((this.Invoice.End_Electric_Meter - this.Invoice.Begin_Electric_Meter) * this.Data.Electric_Price)
        +
        ((this.Invoice.End_Water_Meter - this.Invoice.Begin_Water_Meter) * this.Data.Water_Price))
        / this.Exchange_Rate;
    };

    if(this.Invoice.End_Electric_Meter == '') this.Invoice.End_Electric_Meter = this.Invoice.Begin_Electric_Meter;
    if(this.Invoice.End_Water_Meter == '') this.Invoice.End_Water_Meter = this.Invoice.Begin_Water_Meter;
    if(this.Invoice.End_Date == '') this.Invoice.End_Date = this.Invoice.Begin_Date;

    if(this.Invoice.Begin_Electric_Meter == '') this.Invoice.Begin_Electric_Meter = this.Invoice.End_Electric_Meter;
    if(this.Invoice.Begin_Water_Meter == '') this.Invoice.Begin_Water_Meter = this.Invoice.End_Water_Meter;
    if(this.Invoice.Begin_Date == '') this.Invoice.Begin_Date = this.Invoice.End_Date;

    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        db.executeSql('UPDATE Invoices SET Type = ?, Begin_Date = ?, End_Date = ?, Begin_Electric_Meter = ?, End_Electric_Meter = ?, Begin_Water_Meter = ?, End_Water_Meter = ?, Cash_Due = ?, Cash_Paid = ?, Is_Paid = ? WHERE Invoice_ID = ?', [
          this.Invoice.Type,
          this.Invoice.Begin_Date,
          this.Invoice.End_Date,
          this.Invoice.Begin_Electric_Meter,
          this.Invoice.End_Electric_Meter,
          this.Invoice.Begin_Water_Meter,
          this.Invoice.End_Water_Meter,
          this.Invoice.Cash_Due,
          this.Invoice.Cash_Paid,
          this.Invoice.Is_Paid,
          this.Invoice.Invoice_ID,
        ]).then(res => {
          this.toast.show('Invoice Saved', '5000', 'center').subscribe();
          this.navCtrl.parent.select(2);
        })
        .catch(e => {
          this.toast.show(JSON.stringify(e), '5000', 'center').subscribe();
        });

      }).catch(e => {
      this.toast.show(e, '5000', 'center').subscribe();
    });
  }

}
