# Rental Management

An Ionic native app built for property rental management in Cambodia.
# Features:
- Support IOS, Android, Windows Phone
- Informative
- Property notepad
- Invoice to PDF and Share to any pdf-file supported app
- Keep track of which rooms are paid or partially paid with calculated remaining ammount in USD and KHR
- Automatic Utility usage calculation
- Automatic Invoice type generation
- Utility meter reset support
- Database export and Import in JSON format then able to share to any file supported app like Gmail, Telegram, One Drive, Google Drive and so on.
- Multiple Room Adding support using room prefix
# Prerequisition:
- Node.js, NPM or Yarn
- Ionic 3.9
- Android/IOS/WP SDK
# Installation(android):
- yarn install or npm install
- ionic cordova run android --device or ionic cordova run ios
